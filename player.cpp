#include "player.h"
#include "board.h"
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    this->myBoard = new Board();
    this->mySide = side;
    if(mySide == BLACK)
    {
	this->oppSide = WHITE;
    }
    else
    {
	this->oppSide = BLACK;
    }
    

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

void Player::setBoard(Board * new_board)
{
    myBoard = new_board->copy();
}


int Player::minimax(Board *crtBoard, int depth, Side crtSide, int alpha, int beta)
{
    int bestVal;
    Board *nextBoard;
    int rez;
    Move * m;
    std::vector<Move> possMoves = crtBoard->possibleMoves(crtSide);
    now = time(NULL);
    if (difftime(now, start) >= allot / 1000)
        depth = 0;
    if (depth == 0 || crtBoard->isDone())
    {    
        if (testingMinimax)
        {
            return crtBoard->getHeuristic(true, oppSide);
        }    
        else
        {
	    int nextMove = crtBoard->getHeuristic(false, oppSide);
            return nextMove;
        }
        
    }
    if (!crtBoard->hasMoves(crtSide))
        return minimax(crtBoard, depth - 1, oppSide, alpha, beta);

    if (crtSide == oppSide)
    {
        bestVal = -1000000;
        for (unsigned int i = 0; i < possMoves.size(); i++)
        {	

            nextBoard = crtBoard->copy();
            m = (&possMoves[i]);
            nextBoard->doMove(m, crtSide);
            rez = minimax(nextBoard, depth - 1, mySide, alpha, beta);
            
            if (rez > bestVal)
            {
                bestVal = rez;
            }
            if(alpha < rez)
	    {
		alpha = rez;
            }
	    if(alpha > beta)
	    {
                delete nextBoard;
		return bestVal;
	    }
            
            delete nextBoard;
        }
    }
    else 
    {
        bestVal = 1000000;
        for (unsigned int i = 0; i < possMoves.size(); i++)
        {
			
            nextBoard = crtBoard->copy();
            m = (&possMoves[i]);
            nextBoard->doMove(m, crtSide);

            rez = minimax(nextBoard, depth - 1, oppSide, alpha, beta);
            if (rez < bestVal) 
            {
                bestVal = rez;
            }
            if (rez > bestVal)
            {
                bestVal = rez;
            }
            if(beta > rez)
	    {
		beta = rez;
            }
	    if(alpha > beta)
	    {
                delete nextBoard;
		return bestVal;
	    }
            delete nextBoard;
        }
    }

    return bestVal;
}   
        

        
       
/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    myBoard->doMove(opponentsMove, oppSide);
    if (!myBoard->hasMoves(mySide))
        return NULL;
    Move *m, *rezm = NULL;
    Move *bestm;
    std::vector<Move> possMoves = myBoard->possibleMoves(mySide);
    Board *nextBoard;
    allot = (msLeft - 1000) / (64 - myBoard->countWhite() - myBoard->countBlack());
    if (msLeft == -1)
        allot = 30000;
    start = time(NULL);
    now = start;
    int bestVal = 1000000, rez;
    for (int j = 1; difftime(now, start) < allot / 1000 && j <= 13; j++)
    {
    bestVal = 1000000;
    for (unsigned int i = 0; i < possMoves.size(); i++)
    {
        nextBoard = myBoard->copy();
        m = (&possMoves[i]);
        nextBoard->doMove(m, mySide);
        if (testingMinimax)
           rez = minimax(nextBoard, 1, oppSide, -100000, 100000);
        else
           rez = minimax(nextBoard, j, oppSide, -100000, 100000);

        if (rez < bestVal)
        {
            bestVal = rez;
            bestm = (&possMoves[i]);
        }
        delete nextBoard;
    }
    now = time(NULL);
    if (difftime(now, start) < allot / 1000)
        rezm = new Move(bestm->x, bestm->y);
    }
    myBoard->doMove(rezm, mySide);
    return rezm;
}
