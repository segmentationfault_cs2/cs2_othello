Contributions:

Vlad Lazar:
	- Set up bitBucket Repository
	- Implemented MiniMax algorithm. 
	- Debugged Alpha-Beta pruning.
        - Implemented heuristic function with board weights
        - Implemented iterative deepening taking time left into acount

Yi Yuan: 
	- Implemented helper functions
		Ex: possibleMoves in board class
	- Implemented Alpha-Beta pruning
	- Debugged MiniMax algorithm 

Improvements:
	- Debated adding memoization & dynamic programming
		Would save memory and speed up computation
		of MiniMax. Would allow deeper depth
		settings to be more practical - We couldn't do it due to lack of
                time. 

	- Fine tuned MiniMax Algorithm by adding iterative deepening
          to take advantage of the total time we have at our disposal.
	
	- Fine tuned Alpha Beta Pruning
		Minimizes calculation time needed
		Avoids using up memory for computation
		paths that won't happen in practice

