#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <ctime>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

private:
	Side mySide;
	Side oppSide;
	Board * myBoard;
	
public:
    time_t now, start;
    double allot;
    bool testingMinimax;
    Player(Side side);
    ~Player();
    void setBoard(Board * new_board);
    int minimax(Board *crtBoard, int depth, Side crtSide, int alpha, int beta);
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    
};

#endif
