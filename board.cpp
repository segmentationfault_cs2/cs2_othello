#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
    crtHeuristicB = 0;
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    newBoard->crtHeuristicB = crtHeuristicB;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * @brief Calculates the heuristic for the board and side
 * @param test is a boolean representing whether we are in testMiniMax
 * @param mySide contains the side we want the heuristic for
 * @returns Returns a double representing the heuristic calculated
 */
int Board::getHeuristic(bool test, Side mySide)
{
    if (test)
        if (mySide == BLACK)
            return (2 * black.count() - taken.count());
        else return (taken.count() - 2 * (black.count()));
    else
    {
        if (mySide == BLACK)
            return crtHeuristicB;
        else return (-1) * crtHeuristicB;
        
    }
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

        

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

int Board::getScorePos(int X, int Y)
{
    int val = 1;
    if ((X == 0 && Y == 0) || (X == 7 && Y == 7) ||
        (X == 7 && Y == 0) || (X == 0 && Y == 7))
        val += 50;
    else 
        if ((X == 0 && Y != 1 && Y != 6) ||
           (Y == 0 && X != 1 && X != 6) ||
           (X == 7 && Y != 1 && Y != 6) ||
           (Y == 7 && X != 1 && X != 6))
           val += 5;
         else
             if ((X == 1 && Y == 1) || (X == 6 && Y == 6) ||
                 (X == 1 && Y == 6) || (X == 6 && Y == 1))
                 val -= 11;
             else
                 if ((X == 0 && (Y == 1 || Y == 6)) ||
                     (Y == 0 && (X == 1 || X == 6)) ||
                     (X == 7 && (Y == 1 || Y == 6)) ||
                     (Y == 7 && (X == 1 || X == 6)))
                     val -= 2;

    return val;
}


/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    int sign = -1;
    if (side == BLACK)
        sign = 1;
    crtHeuristicB += (sign) * getScorePos(X, Y);
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    crtHeuristicB += 2 * (sign) * getScorePos(x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Returns a vector of possible Moves for a side
 */
std::vector<Move> Board::possibleMoves(Side side)
{
        std::vector <Move> possMoves;
	if(this->hasMoves(side) == true)
        {
		for(int i = 0; i < 8; i++)
		{
			for(int j=0; j < 8; j++)
			{
				Move temp = Move(i,j);
                                Move * m = &temp;
				if(this->checkMove(m, side) == true)
				{
					possMoves.push_back(temp);
				}
			}
		}
		
	}
	return possMoves;
}


/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
